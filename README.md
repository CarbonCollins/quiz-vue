# quiz-vue

A quiz application using Vue

## Table of Contents

* [Usage](#usage)
  * [Installation](#installation)
  * [Serving](#serving)
  * [building](#building)

## Usage

### Installation

To run this application you need to install its dependencies which can be done using:
```bash
npm install
```

### Serving

To start the dev webserver run:
```bash
npm run serve
```

and then open your browser to the address in the response which is likely to be [http://localhost:8080](http://localhost:8080)

### Building

Building the application as a distribution can be done by running:
```bash
npm run build
```
