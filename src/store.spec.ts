import Vuex from 'vuex';
import { getters, mutations, store } from './store';

import { expect } from 'chai';
import { Locale } from './models/locale';

describe('store', () => {
  it('should export instance of Vuex store', () => {
    expect(store).to.be.instanceOf(Vuex.Store);
  });

  it('should initialise with default state', () => {
    expect(store.state).to.be.an('object');
    expect(store.state.locale).to.be.instanceOf(Locale);
    expect(store.state.quiz).to.be.equal(null);
  });

  describe('mutations', () => {
    describe('answerActiveQuestion', () => {
      it('should not consume lifeline if not quiz', () => {
        const mockState = {};

        mutations.answerActiveQuestion(mockState as any, { answer: 'test' });

        expect(mockState).to.deep.equal({});
      });

      it('should call answerActiveQuestion if quiz is running', () => {
        let result = null;
        const mockState = { quiz: {
            answerActiveQuestion: (answer: string) => { result = answer; },
          }};

        mutations.answerActiveQuestion(mockState as any, { answer: 'test' });

        expect(result).to.deep.equal('test');
      });

      it('should send stats with answer if quiz is running', () => {
        let result = null;
        const mockState = { quiz: {
            answerActiveQuestion: (answer: string, stats: object) => { result = stats; },
          }};

        mutations.answerActiveQuestion(mockState as any, { answer: 'test', stats: { test: true } });

        expect(result).to.deep.equal({ test: true });
      });
    });

    describe('consumeLifeline', () => {
      it('should not consume lifeline if not quiz', () => {
        const mockState = {};

        mutations.consumeLifeline(mockState as any, 'test');

        expect(mockState).to.deep.equal({});
      });

      it('should consume lifeline if quiz is running', () => {
        let result = null;
        const mockState = { quiz: {
            consumeLifeline: (lifeline: string) => { result = lifeline; },
          }};

        mutations.consumeLifeline(mockState as any, 'test');

        expect(result).to.be.equal('test');
      });
    });

    describe('createQuiz', () => {
      it('should set quiz input', () => {
        const mockState = { quiz: null };

        mutations.createQuiz(mockState as any, true as any);

        expect(mockState.quiz).to.be.equal(true);
      });
    });

    describe('reset', () => {
      it('should set quiz to null', () => {
        const mockState = { quiz: true };

        mutations.reset(mockState as any);

        expect(mockState.quiz).to.be.equal(null);
      });
    });
  });

  describe('getters', () => {
    describe('countryCode', () => {
      it('should return countrycode from state', () => {
        const mockState = {
          locale: {
            countryCode: 'test',
          },
        };

        expect(getters.countryCode(mockState as any)).to.be.equal('test');
      });
    });

    describe('currentQuestion', () => {
      it('should return activeQuestion from state', () => {
        const mockState = {
          quiz: {
            getActiveQuestion: () => 'test',
          },
        };

        expect(getters.currentQuestion(mockState as any)).to.be.equal('test');
      });

      it('should return null if no quiz running', () => {
        const mockState = {};

        expect(getters.currentQuestion(mockState as any)).to.be.equal(null);
      });
    });

    describe('fiftyfiftyAvailable', () => {
      it('should return true from state', () => {
        const mockState = {
          quiz: {
            hasLifelineAvailable: () => true,
          },
        };

        expect(getters.fiftyfiftyAvailable(mockState as any)).to.be.equal(true);
      });

      it('should return false from state', () => {
        const mockState = {
          quiz: {
            hasLifelineAvailable: () => false,
          },
        };

        expect(getters.fiftyfiftyAvailable(mockState as any)).to.be.equal(false);
      });

      it('should return false if no quiz running', () => {
        const mockState = {};

        expect(getters.fiftyfiftyAvailable(mockState as any)).to.be.equal(false);
      });
    });

    describe('plustenAvailable', () => {
      it('should return true from state', () => {
        const mockState = {
          quiz: {
            hasLifelineAvailable: () => true,
          },
        };

        expect(getters.plustenAvailable(mockState as any)).to.be.equal(true);
      });

      it('should return false from state', () => {
        const mockState = {
          quiz: {
            hasLifelineAvailable: () => false,
          },
        };

        expect(getters.plustenAvailable(mockState as any)).to.be.equal(false);
      });

      it('should return false if no quiz running', () => {
        const mockState = {};

        expect(getters.plustenAvailable(mockState as any)).to.be.equal(false);
      });
    });

    // repeat for other getters
  });
});
