import { RawQuestionFormat } from '@app/models/question';

import { questions as biscuitQuestions } from './biscuits';
import { questions as landQuestions } from './land';
import { questions as townQuestions } from './towns';

export const questions: RawQuestionFormat[] = [
  ...biscuitQuestions,
  ...townQuestions,
  ...landQuestions,
];
