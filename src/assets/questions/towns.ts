import { RawQuestionFormat } from '@app/models/question';

export const questions: RawQuestionFormat[] = [
  {
    correctAnswers: [
      {
        'en-GB': 'Thimbleton',
        'sv-SE': 'Thimbleton',
      },
    ],
    question: {
      'en-GB': 'Which of the following British towns does not exist?',
      'sv-SE': 'Vilken av följande brittiska städer existerar inte?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Barton in the Beans',
        'sv-SE': 'Barton in the Beans',
      },
      {
        'en-GB': 'Curry Mallet',
        'sv-SE': 'Curry Mallet',
      },
      {
        'en-GB': 'Puddletown',
        'sv-SE': 'Puddletown',
      },
      {
        'en-GB': 'Westward Ho!',
        'sv-SE': 'Westward Ho!',
      },
      {
        'en-GB': 'Marsh Gibbon',
        'sv-SE': 'Marsh Gibbon',
      },
      {
        'en-GB': 'Bitchfield',
        'sv-SE': 'Bitchfield',
      },
      {
        'en-GB': 'Wetwang',
        'sv-SE': 'Wetwang',
      },
      {
        'en-GB': 'Great Snoring',
        'sv-SE': 'Great Snoring',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'Boghead',
        'sv-SE': 'Boghead',
      },
    ],
    question: {
      'en-GB': 'Which town is located in Scotland?',
      'sv-SE': 'Vilken stad ligger i Skottland?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Weedon',
        'sv-SE': 'Weedon',
      },
      {
        'en-GB': 'Swell',
        'sv-SE': 'Swell',
      },
      {
        'en-GB': 'Splatt',
        'sv-SE': 'Splatt',
      },
      {
        'en-GB': 'Horncastle',
        'sv-SE': 'Horncastle',
      },
      {
        'en-GB': 'Cat’s Ash',
        'sv-SE': 'Cat’s Ash',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'St Davids',
        'sv-SE': 'St Davids',
      },
    ],
    question: {
      'en-GB': 'Which is the smalest city within the UK?',
      'sv-SE': 'Vilken är den minsta staden i Storbrittannien?',
    },
    wrongAnswers: [
      {
        'en-GB': 'City of London',
        'sv-SE': 'City of London',
      },
      {
        'en-GB': 'Bangor',
        'sv-SE': 'Bangor',
      },
      {
        'en-GB': 'St Asaph',
        'sv-SE': 'St Asaph',
      },
      {
        'en-GB': 'Wells',
        'sv-SE': 'Wells',
      },
      {
        'en-GB': 'Stirling',
        'sv-SE': 'Stirling',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'City of London',
        'sv-SE': 'City of London',
      },
    ],
    question: {
      'en-GB': 'Which city has the smallest council area in the UK?',
      'sv-SE': 'Vilken stad har det minsta geografiska kommunområdet i Storbrittannien?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Wells',
        'sv-SE': 'Wells',
      },
      {
        'en-GB': 'St Asaph',
        'sv-SE': 'St Asaph',
      },
      {
        'en-GB': 'Bangor',
        'sv-SE': 'Bangor',
      },
      {
        'en-GB': 'Ripon',
        'sv-SE': 'Ripon',
      },
      {
        'en-GB': 'City of Canterbury',
        'sv-SE': 'City of Canterbury',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'Rochester',
        'sv-SE': 'Rochester',
      },
    ],
    question: {
      'en-GB': 'Which town lost its city status in 1998?',
      'sv-SE': 'Vilken av följande orter förlorade sin status som stad år 1998?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Milton Keynes',
        'sv-SE': 'Milton Keynes',
      },
      {
        'en-GB': 'Northampton',
        'sv-SE': 'Northampton',
      },
      {
        'en-GB': 'Luton',
        'sv-SE': 'Luton',
      },
      {
        'en-GB': 'Reading',
        'sv-SE': 'Reading',
      },
    ],
  },
];
