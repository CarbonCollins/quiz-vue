import { RawQuestionFormat } from '@app/models/question';

export const questions: RawQuestionFormat[] = [
  {
    correctAnswers: [
      {
        'en-GB': 'Great Britian',
        'sv-SE': 'Great Britian',
      },
    ],
    question: {
      'en-GB': 'Which of the following includes England, Scotland, Wales but not Northern Irelend?',
      'sv-SE': 'Vilket av följande begrepp inkluderar England, Skottland och Wales, men inte Nordirland?',
    },
    wrongAnswers: [
      {
        'en-GB': 'British Isles',
        'sv-SE': 'British Isles',
      },
      {
        'en-GB': 'United Kingdom',
        'sv-SE': 'United Kingdom',
      },
      {
        'en-GB': 'Britian',
        'sv-SE': 'Britian',
      },
      {
        'en-GB': 'United Kingdom of Great Britain and Northern Ireland',
        'sv-SE': 'United Kingdom of Great Britain and Northern Ireland',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'Morecambe Bay',
        'sv-SE': 'Morecambe Bay',
      },
    ],
    question: {
      'en-GB': 'Where is the center of the United Kingdom of Great Britain and Northern Ireland?',
      'sv-SE': 'Var är den geografiska mittpunkten i Storbrittannien',
    },
    wrongAnswers: [
      {
        'en-GB': 'Calderstones',
        'sv-SE': 'Calderstones',
      },
      {
        'en-GB': 'Cwmystwyth',
        'sv-SE': 'Cwmystwyth',
      },
      {
        'en-GB': 'Church Flatts Farm',
        'sv-SE': 'Church Flatts Farm',
      },
      {
        'en-GB': 'Whitendale Hanging Stones',
        'sv-SE': 'Whitendale Hanging Stones',
      },
    ],
  },
];
