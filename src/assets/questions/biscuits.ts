import { RawQuestionFormat } from '@app/models/question';

export const questions: RawQuestionFormat[] = [
  {
    correctAnswers: [
      {
        'en-GB': 'Viennese Square',
        'sv-SE': 'Viennese Square',
      },
      {
        'en-GB': 'Purple Wafer',
        'sv-SE': 'Purple Wafer',
      },
    ],
    question: {
      'en-GB': 'Which of these British biscuits does not exist?',
      'sv-SE': 'Vilken av dessa brittiska kakor finns inte?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Rich Tea',
        'sv-SE': 'Rich Tea',
      },
      {
        'en-GB': 'Jammie Dodger',
        'sv-SE': 'Jammie Dodger',
      },
      {
        'en-GB': 'Malted Milk',
        'sv-SE': 'Malted Milk',
      },
      {
        'en-GB': 'Nice',
        'sv-SE': 'Nice',
      },
      {
        'en-GB': 'Hobnob',
        'sv-SE': 'Hobnob',
      },
      {
        'en-GB': 'Garibaldi',
        'sv-SE': 'Garibaldi',
      },
      {
        'en-GB': 'Bourbon',
        'sv-SE': 'Bourbon',
      },
      {
        'en-GB': 'Party Rings',
        'sv-SE': 'Party Rings',
      },
      {
        'en-GB': 'Chocolate Rounds',
        'sv-SE': 'Chocolate Rounds',
      },
      {
        'en-GB': 'Wagon Wheels',
        'sv-SE': 'Wagon Wheels',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': '1830',
        'sv-SE': '1830',
      },
    ],
    question: {
      'en-GB': 'When was McVitie`s founded?',
      'sv-SE': 'När grundades McVitie`s?',
    },
    wrongAnswers: [
      {
        'en-GB': '1827',
        'sv-SE': '1827',
      },
      {
        'en-GB': '1835',
        'sv-SE': '1835',
      },
      {
        'en-GB': '1820',
        'sv-SE': '1820',
      },
      {
        'en-GB': '1841',
        'sv-SE': '1841',
      },
      {
        'en-GB': '1842',
        'sv-SE': '1842',
      },
      {
        'en-GB': '1823',
        'sv-SE': '1823',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': '18',
        'sv-SE': '18',
      },
    ],
    question: {
      'en-GB': 'In 2012, in Broadstairs, Kent, Elliott Allen claimed a world record. How many digestive biscuits did he break with one karate chop?',
      'sv-SE': 'År 2012 i Broadstairs, Kent satte Elliot Allen världsrekord. Hur många digestivekex lyckades han slå sönder med ett karateslag?',
    },
    wrongAnswers: [
      {
        'en-GB': '10',
        'sv-SE': '10',
      },
      {
        'en-GB': '52',
        'sv-SE': '52',
      },
      {
        'en-GB': '12',
        'sv-SE': '12',
      },
      {
        'en-GB': '19',
        'sv-SE': '19',
      },
      {
        'en-GB': '23',
        'sv-SE': '23',
      },
      {
        'en-GB': '34',
        'sv-SE': '34',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'Wagon Wheel',
        'sv-SE': 'Wagon Wheel',
      },
    ],
    question: {
      'en-GB': 'Which round biscuit is filled with marshmallow and coated in chocolate, and also has had a variety with a layer of red jam added?',
      'sv-SE': 'Vilken typ av runt kex är fyllt med marshmallow och täckt med choklad, och finns i en variant med ett lager av röd sylt?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Blue Riband',
        'sv-SE': 'Blue Riband',
      },
      {
        'en-GB': 'Breakaway',
        'sv-SE': 'Breakaway',
      },
      {
        'en-GB': 'Rocky Road',
        'sv-SE': 'Rocky Road',
      },
      {
        'en-GB': 'Taxi',
        'sv-SE': 'Taxi',
      },
      {
        'en-GB': 'Penguin',
        'sv-SE': 'Penguin',
      },
      {
        'en-GB': 'Club',
        'sv-SE': 'Club',
      },
    ],
  },
  {
    correctAnswers: [
      {
        'en-GB': 'Almond',
        'sv-SE': 'Mandel',
      },
    ],
    question: {
      'en-GB': 'Which one of the following is not a Club biscuit flavour?',
      'sv-SE': 'Vilken av följande finns inte som smak på Club kex?',
    },
    wrongAnswers: [
      {
        'en-GB': 'Mint',
        'sv-SE': 'Mynta',
      },
      {
        'en-GB': 'Orange',
        'sv-SE': 'Apelsin',
      },
      {
        'en-GB': 'Fruit',
        'sv-SE': 'Frukt',
      },
      {
        'en-GB': 'Honeycomb',
        'sv-SE': 'Vaxkaka',
      },
    ],
  },
];
