import { Question } from './question';

import { expect } from 'chai';

const mockQuestion = {
  correctAnswers: [{ 'en-GB': 'test' }],
  question: { 'en-GB': 'test?' },
  wrongAnswers: [
    { 'en-GB': 'test1' },
    { 'en-GB': 'test2' },
    { 'en-GB': 'test4' },
  ],
};

describe('Question', () => {
  describe('constructor', () => {
    it('should construct question from raw data', () => {
      const result: Question = new Question(mockQuestion);

      expect(result).to.be.instanceOf(Question);
    });
  });

  describe('getQuestion', () => {
    it('should return question string for specefic locale', () => {
      const result: Question = new Question(mockQuestion);

      expect(result.getQuestion('en-GB')).to.deep.equal(mockQuestion.question['en-GB']);
    });
  });

  describe('getChoices', () => {
    it('should return an array of choices for specefic locale', () => {
      const result: Question = new Question(mockQuestion);
      const choices = result.getChoices('en-GB');

      expect(choices).to.be.an('array');
      expect(choices).to.have.all.members([
        ...mockQuestion.wrongAnswers.map((o) => o['en-GB']),
        ...mockQuestion.correctAnswers.map((o) => o['en-GB']),
      ]);
    });
  });

  describe('isChoiceCorrect', () => {
    it('should return false if no answer has been submitted', () => {
      const result: Question = new Question(mockQuestion);

      expect(result.isChoiceCorrect()).to.be.equal(false);
    });

    it('should return false if answer is wrong', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('wrong');

      expect(result.isChoiceCorrect()).to.be.equal(false);
    });

    it('should return true if answer is correct', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('test');

      expect(result.isChoiceCorrect()).to.be.equal(true);
    });
  });

  describe('answered', () => {
    it('should return false if no answer has been submitted', () => {
      const result: Question = new Question(mockQuestion);

      expect(result.answered()).to.be.equal(false);
    });

    it('should return true if answer is submitted', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('wrong');

      expect(result.answered()).to.be.equal(true);
    });
  });

  describe('answerQuestion', () => {
    it('should submit the awnser into the question', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('wrong');

      expect(result.answered()).to.be.equal(true);
    });

    it('should save statistics if supplied', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('wrong', {
        timeTaken: 12,
      });

      expect(result.answered()).to.be.equal(true);
      expect(result.getTimeTaken()).to.be.equal(12);
    });
  });

  describe('reduceAvailableChoices', () => {
    it('should reduce the amount of choices available in the question by 2', () => {
      const result: Question = new Question(mockQuestion);

      const preChoices = result.getChoices('en-GB');

      result.reduceAvailableChoices();

      const postChoices = result.getChoices('en-GB');

      expect(preChoices.length).to.be.an('number');
      expect(postChoices.length).to.be.an('number');
      expect(postChoices.length).to.be.lessThan(preChoices.length);
      expect(preChoices.length - postChoices.length).to.be.equal(2);
    });

    it('should reduce the amount of choices available in the question by custom amount', () => {
      const result: Question = new Question(mockQuestion);

      const preChoices = result.getChoices('en-GB');

      result.reduceAvailableChoices(1);

      const postChoices = result.getChoices('en-GB');

      expect(preChoices.length).to.be.an('number');
      expect(postChoices.length).to.be.an('number');
      expect(postChoices.length).to.be.lessThan(preChoices.length);
      expect(preChoices.length - postChoices.length).to.be.equal(1);
    });
  });

  describe('getTimeTaken', () => {
    it('should return 0 if question has not been awnsered with stats', () => {
      const result: Question = new Question(mockQuestion);

      expect(result.getTimeTaken()).to.be.equal(0);
    });

    it('should return amount if question has been awnsered with stats', () => {
      const result: Question = new Question(mockQuestion);

      result.answerQuestion('test', { timeTaken: 13 });

      expect(result.getTimeTaken()).to.be.equal(13);
    });
  });
});
