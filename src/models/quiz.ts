import { Question, Stats } from '@app/models/question';

export interface Lifelines {
  fiftyfifty: boolean;
  plusten: boolean;
}

export class Quiz {
  private questionPool: Question[];
  private answeredQuestions: Question[];
  private activeQuestion: Question | null;

  private lifelines: Lifelines = {
    fiftyfifty: true,
    plusten: true,
  };

  constructor(questions: Question[]) {
    this.questionPool = [...questions];
    this.answeredQuestions = [];
    this.activeQuestion = null;
  }

  public getActiveQuestion() {
    if (!this.activeQuestion) {
      this.activeQuestion = this.questionPool.pop() || null;
    }

    return this.activeQuestion;
  }

  public answerActiveQuestion(answer: string, stats: Stats): void {
    if (this.activeQuestion) {
      this.activeQuestion.answerQuestion(answer, stats);

      this.answeredQuestions.push(this.activeQuestion);
      this.activeQuestion = null;
    }
  }

  public hasQuestionsLeft(): boolean {
    return (this.questionPool.length > 0 || this.activeQuestion !== null);
  }

  public hasLifelineAvailable(lifeline: keyof Lifelines): boolean {
    return this.lifelines[lifeline];
  }

  public consumeLifeline(lifeline: keyof Lifelines): void {
    this.lifelines[lifeline] = false;

    if (lifeline === 'fiftyfifty' && this.activeQuestion) {
      this.activeQuestion.reduceAvailableChoices();
    }
  }

  public answers() {
    return this.answeredQuestions;
  }
}
