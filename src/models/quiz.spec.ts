import { Question } from './question';
import { Quiz } from './quiz';

import { expect } from 'chai';

const mockQuestion: Question = new Question({
  correctAnswers: [{ 'en-GB': 'test' }],
  question: { 'en-GB': 'test?' },
  wrongAnswers: [
    { 'en-GB': 'test1' },
    { 'en-GB': 'test2' },
    { 'en-GB': 'test4' },
  ],
});

describe('Quiz', () => {
  describe('constructor', () => {
    it('should construct question from raw data', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result).to.be.instanceOf(Quiz);
    });
  });

  describe('getActiveQuestion', () => {
    it('should get question from pool when no active question is available', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result.getActiveQuestion()).to.be.instanceOf(Question);
    });

    it('should return null when no active question is available', () => {
      const result: Quiz = new Quiz([]);

      expect(result.getActiveQuestion()).to.be.equal(null);
    });
  });

  describe('hasQuestionsLeft', () => {
    it('should return true when there are questions still available', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result.hasQuestionsLeft()).to.be.equal(true);
    });

    it('should return false when there are no questions still available', () => {
      const result: Quiz = new Quiz([]);

      expect(result.hasQuestionsLeft()).to.be.equal(false);
    });
  });

  describe('hasLifelineAvailable', () => {
    it('should return true when plusten lifeline is available', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result.hasLifelineAvailable('plusten')).to.be.equal(true);
      expect(result.hasLifelineAvailable('fiftyfifty')).to.be.equal(true);
    });

    it('should return false when plusten lifeline is not available', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      result.consumeLifeline('plusten');

      expect(result.hasLifelineAvailable('plusten')).to.be.equal(false);
      expect(result.hasLifelineAvailable('fiftyfifty')).to.be.equal(true);
    });
  });

  describe('consumeLifeline', () => {
    it('should consume lifeline', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result.hasLifelineAvailable('plusten')).to.be.equal(true);

      result.consumeLifeline('plusten');

      expect(result.hasLifelineAvailable('plusten')).to.be.equal(false);
    });
  });

  describe('answers', () => {
    it('should return empty array when no questions have been answered', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      expect(result.answers()).to.be.an('array').and.to.have.lengthOf(0);
    });

    it('should return populated array when questions have been answered', () => {
      const result: Quiz = new Quiz([mockQuestion]);

      result.getActiveQuestion();
      result.answerActiveQuestion('test', {});

      expect(result.answers()).to.be.an('array').and.to.have.lengthOf(1);
    });
  });
});
