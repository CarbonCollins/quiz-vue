const languageCode: unique symbol = Symbol('languageCode');
const countryCode: unique symbol = Symbol('countryCode');

export class Locale {
  private [languageCode]: string;
  private [countryCode]: string;

  constructor(localeString: string) {
    this.updateLocale(localeString);
  }

  get languageCode(): string {
    return this[languageCode];
  }

  set languageCode(value: string) {
    this[languageCode] = value.toLowerCase();
  }

  get countryCode(): string {
    return this[countryCode];
  }

  set countryCode(value: string) {
    this[countryCode] = value.toUpperCase();
  }

  public updateLocale(localeString: string) {
    if (localeString.includes('-')) {
      const parts = localeString.split('-');

      this[languageCode] = parts[0];
      this[countryCode] = parts[1];
    } else {
      this[languageCode] = localeString;
    }
  }

  public toString(): string {
    return `${this.languageCode}-${this.countryCode}`;
  }
}
