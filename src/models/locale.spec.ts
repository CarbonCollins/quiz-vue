import { Locale } from './locale';

import { expect } from 'chai';

describe('Locale', () => {
  it('should construct with input locale', () => {
    const result: Locale = new Locale('en-GB');

    expect(result).to.be.instanceOf(Locale);
  });

  describe('languageCode', () => {
    it('should get the language code', () => {
      const result: Locale = new Locale('en-GB');

      expect(result.languageCode).to.be.equal('en');
    });

    it('should set the language code in lowercase', () => {
      const result: Locale = new Locale('en-GB');

      result.languageCode = 'SV';

      expect(result.languageCode).to.be.equal('sv');
    });
  });

  describe('countryCode', () => {
    it('should get the country code', () => {
      const result: Locale = new Locale('en-GB');

      expect(result.countryCode).to.be.equal('GB');
    });

    it('should set the country code in uppercase', () => {
      const result: Locale = new Locale('en-GB');

      result.countryCode = 'se';

      expect(result.countryCode).to.be.equal('SE');
    });
  });

  describe('updateLocale', () => {
    it('should set the language code', () => {
      const result: Locale = new Locale('en-GB');

      result.updateLocale('sv-SE');

      expect(result.languageCode).to.be.equal('sv');
    });

    it('should set the country code', () => {
      const result: Locale = new Locale('en-GB');

      result.updateLocale('sv-SE');

      expect(result.countryCode).to.be.equal('SE');
    });
  });

  describe('toString', () => {
    it('should return stringified locale', () => {
      const result: Locale = new Locale('en-GB');

      expect(result.toString()).to.be.equal('en-GB');
    });
  });
});
