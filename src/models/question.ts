import { shuffleArray } from '@app/helpers/shuffle';

import { Locale } from '@app/models/locale';

interface LocaleString {
  [K: string]: string;
}

export interface Choice {
  text: string;
  active: boolean;
}

export interface Stats {
  timeTaken?: number;
}

export interface RawQuestionFormat {
  correctAnswers: LocaleString[];
  question: LocaleString;
  wrongAnswers: LocaleString[];
}

export class Question {
  private question: LocaleString;
  private correctAnswer: LocaleString;
  private wrongAnswers: LocaleString[];
  private choiceReduction: number =  0;

  private chosenAnswer: string | null = null;
  private stats: Stats = {};

  constructor(questiondata: RawQuestionFormat) {
    this.question = questiondata.question;

    this.correctAnswer = shuffleArray(questiondata.correctAnswers)[0];
    this.wrongAnswers = shuffleArray(questiondata.wrongAnswers).slice(0, 4);
  }

  public getQuestion(locale: Locale | string): string {
    return this.question[locale.toString()] || '';
  }

  public getChoices(locale: Locale | string): string[] {
    const choices = shuffleArray([...this.wrongAnswers, this.correctAnswer])
      .map((answers: LocaleString) => answers[locale.toString()]);

    choices.splice(0, this.choiceReduction);

    return choices;
  }

  public isChoiceCorrect(): boolean {
    return (this.chosenAnswer)
      ? Object.values(this.correctAnswer).includes(this.chosenAnswer)
      : false;
  }

  public answered(): boolean {
    return !!(this.chosenAnswer && this.chosenAnswer !== '');
  }

  public answerQuestion(choice: string, stats: Stats = {}) {
    this.chosenAnswer = choice;

    if (stats) {
      this.stats = stats;
    }
  }

  public reduceAvailableChoices(amount: number = 2) {
    this.choiceReduction += amount;
  }

  public getTimeTaken() {
    return (this.stats && this.stats.timeTaken)
      ? this.stats.timeTaken
      : 0;
  }
}
