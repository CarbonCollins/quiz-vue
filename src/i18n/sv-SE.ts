import { LocaleMessageObject } from 'vue-i18n';

export const messages: LocaleMessageObject = {
  averageTimeTaken: 'Genomsnittlig tid per fråga',
  fastestTimetaken: 'Snabbaste svaret tog',
  fiftyfifty: '50/50',
  gameInfo1: 'Besvara varje fråga inom 15 sekunder',
  gameInfo2: 'Om du inte svarar inom 15 sekunder hoppar du automatiskt över frågan',
  gameInfo3: 'Du kommer att få 5 alternativ varav ett är korrekt',
  gameInfo4: 'Du kommer att få några livlinor, men de kan bara användas en gång',
  gameInfoHeader: 'Spelet är enkelt:',
  homeDescription: 'Välkommen till Quiz. Vi kommer att ställa några frågor och du får försöka gissa rätt svar',
  homeTitle: 'Starta spelet',
  lifelines: 'Livlinor: ',
  plusten: '+10s',
  results: 'Resultat',
  score: 'Poäng',
  secondUnit: 's',
  start: 'Starta',
  startAgain: 'Spela igen',
  totalTimeTaken: 'Sammanlagd tid',
  unansweredQuestions: 'Obesvarade frågor',
};
