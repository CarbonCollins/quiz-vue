import { LocaleMessageObject } from 'vue-i18n';

export const messages: LocaleMessageObject = {
  averageTimeTaken: 'Average time per question',
  fastestTimetaken: 'Fastest answer took',
  fiftyfifty: '50/50',
  gameInfo1: 'Answer each question within 15 seconds',
  gameInfo2: 'If you fail to answer within 15 seconds you will skip that question',
  gameInfo3: 'You will be given a choice of 5 answers where one is correct',
  gameInfo4: 'There will be some lifelines available but they can only be used once',
  gameInfoHeader: 'The game is simple:',
  homeDescription: 'Welcome to Quiz where we will ask a few questions and you try to guess the correct answer.',
  homeTitle: 'Start Quiz',
  plusten: '+10s',
  results: 'Results',
  score: 'Score',
  secondUnit: 's',
  start: 'Start',
  startAgain: 'Start Again',
  totalTimeTaken: 'Total time taken',
  unansweredQuestions: 'Unanswerd questions',
};
