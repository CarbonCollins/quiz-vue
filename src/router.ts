import Vue from 'vue';
import Router, { NavigationGuard, Route } from 'vue-router';

import { store } from '@app/store';

import Page404 from '@app/views/404.vue';
import PageHome from '@app/views/home.vue';
import PageQuiz from '@app/views/quiz.vue';
import PageResult from '@app/views/result.vue';

Vue.use(Router);

const isInQuiz: NavigationGuard = async (to: Route, from: Route, next) => {
  if (store.getters.quiz && store.getters.quiz.hasQuestionsLeft()) {
    next();
    return;
  }

  next('/');
};

const hasResults: NavigationGuard = async (to: Route, from: Route, next) => {
  if (store.getters.quiz && !store.getters.quiz.hasQuestionsLeft()) {
    next();
    return;
  }

  next('/');
};

const isNotInQuiz: NavigationGuard = async (to: Route, from: Route, next) => {
  if (!store.getters.quiz) {
    next();
    return;
  }

  next('/quiz');
};

export const router = new Router({
  base: process.env.BASE_URL,
  mode: 'history',
  routes: [
    {
      beforeEnter: isNotInQuiz,
      component: PageHome,
      path: '/',
    },
    {
      beforeEnter: isInQuiz,
      component: PageQuiz,
      path: '/quiz',
    },
    {
      beforeEnter: hasResults,
      component: PageResult,
      path: '/result',
    },
    {
      component: Page404,
      path: '*',
    },
  ],
});
