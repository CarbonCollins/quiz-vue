import Vue from 'vue';
import Vuex, { ActionContext } from 'vuex';

import { Locale } from '@app/models/locale';
import { Lifelines, Quiz } from '@app/models/quiz';

Vue.use(Vuex);

export interface AppStore {
  locale: Locale;
  quiz: Quiz | null;
}

export const getters = {
  countryCode: (state: AppStore) => state.locale.countryCode,
  currentQuestion: (state: AppStore) => {
    return (state.quiz)
      ? state.quiz.getActiveQuestion()
      : null;
  },
  fiftyfiftyAvailable: (state: AppStore) => (state.quiz) ? state.quiz.hasLifelineAvailable('fiftyfifty') : false,
  languageCode: (state: AppStore) => state.locale.languageCode,
  locale: (state: AppStore) => state.locale.toString(),
  plustenAvailable: (state: AppStore) => (state.quiz) ? state.quiz.hasLifelineAvailable('plusten') : false,
  quiz: (state: AppStore) => state.quiz,
  quizAnswers: (state: AppStore) => (state.quiz) ? state.quiz.answers() : [],
};

export const mutations = {
  answerActiveQuestion: (state: AppStore, { answer, stats }: any) => {
    if (state.quiz) {
      state.quiz.answerActiveQuestion(answer, stats);
    }
  },
  consumeLifeline: (state: AppStore, lifeline: string) => {
    if (state.quiz) {
      state.quiz.consumeLifeline(lifeline as keyof Lifelines);
    }
  },
  createQuiz: (state: AppStore, quiz: Quiz) => {
    state.quiz = quiz;
  },
  reset: (state: AppStore) => {
    state.quiz = null;
  },
};

export const store: any = new Vuex.Store<AppStore>({
  actions: {
    createQuiz(context: ActionContext<AppStore, AppStore>, quiz: Quiz) {
      context.commit('createQuiz', quiz);
    },
    answerActiveQuestion(context: ActionContext<AppStore, AppStore>, { answer, stats }) {
      context.commit('answerActiveQuestion', { answer, stats });
    },
    consumeLifeline(context: ActionContext<AppStore, AppStore>, lifeline: string) {
      context.commit('consumeLifeline', lifeline);
    },
    reset(context: ActionContext<AppStore, AppStore>) {
      context.commit('reset');
    },
  },
  getters,
  mutations,
  state: {
    locale: new Locale('en-GB'),
    quiz: null,
  },
});
