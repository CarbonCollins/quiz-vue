import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { messages as enGBMessages} from '@app/i18n/en-GB';
import { messages as svSEMessages} from '@app/i18n/sv-SE';

Vue.use(VueI18n);

export const i18n = new VueI18n({
  fallbackLocale: 'en-GB',
  locale: 'en-GB',
  messages: {
    'en-GB': enGBMessages,
    'sv-SE': svSEMessages,
  },
});

const loadedLanguages: string[] = ['en-GB', 'sv-SE'];

function setI18nLanguage(language: string): string {
  i18n.locale = language;
  const htmlelement = document.querySelector('html');
  if (htmlelement) {
    htmlelement.setAttribute('lang', language);
  }
  return language;
}

export function loadLanguageAsync(language: string): Promise<string> {
  if (i18n.locale === language) {
    return Promise.resolve(setI18nLanguage(language));
  }

  if (loadedLanguages.includes(language)) {
    return Promise.resolve(setI18nLanguage(language));
  }

  return import(/* webpackChunkName: "lang-[request]" */ `@app/i18n/${language}.ts`)
    .then((languageFile) => {
      i18n.setLocaleMessage(language, languageFile.messages);
      loadedLanguages.push(language);

      return setI18nLanguage(language);
    });
}
