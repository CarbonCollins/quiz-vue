import { shallowMount } from '@vue/test-utils';
import Button from './button.vue';

import { expect } from 'chai';

describe('Button', () => {
  it('renders button with a link', () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        href: 'http://example.com',
      },
    });

    expect(wrapper.contains('a')).to.be.equal(true);
    expect(wrapper.find('a').props().href).to.be.equal('http://example.com');
  });

  it('renders a disabled button', () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        disabled: true,
      },
    });

    expect(wrapper.contains('a')).to.be.equal(false);
    expect(wrapper.find('.button-disabled').exists()).to.not.be.equal(false);
  });

  it('should not render an icon', () => {
    const wrapper = shallowMount(Button, {
      propsData: {},
    });

    expect(wrapper.find('.button-icon').exists()).to.be.equal(false);
  });

  it('should render an icon', () => {
    const wrapper = shallowMount(Button, {
      propsData: {
        icon: 'test',
      },
    });

    expect(wrapper.find('.button-icon').exists()).to.not.be.equal(false);
  });
});
