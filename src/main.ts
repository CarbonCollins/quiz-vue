import Vue from 'vue';

import App from '@app/app.vue';
import { router } from '@app/router';
import { i18n } from '@app/setup/i18n';
import { store } from '@app/store';

import 'typeface-roboto/index.css';

Vue.config.productionTip = false;

new Vue({
  i18n,
  render: (h: any) => h(App),
  router,
  store,
}).$mount('#app');
