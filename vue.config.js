const path = require('path');

module.exports = {
  chainWebpack: (config) => {
    config.resolve.alias.set('@app', path.resolve(__dirname, 'src/'));
  }
};
